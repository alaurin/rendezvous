import {createStore, applyMiddleware, compose} from "redux"
import ReduxThunk from "redux-thunk"
import {Router, Route, IndexRedirect, browserHistory} from "react-router"
import {routerMiddleware} from "react-router-redux"
import applicationReducer from '../reducers/applicationReducer'

export default function configureStore(initialState) {
	const store = createStore(applicationReducer,
		compose(
	   		// support actions returning promises
	        applyMiddleware(ReduxThunk),
	        // router
	        applyMiddleware(routerMiddleware(browserHistory)),
	        // redux devtools browser extension
	        window.devToolsExtension ? window.devToolsExtension() : f => f
   		)
   	);

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers').default
      store.replaceReducer(nextReducer)
    })
  }

  return store
}