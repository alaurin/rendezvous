import * as A from '../actions/action-type'

const defaultStartState = { isLoggedIn: false, 
                            isFetching: false, 
                            token: null,
                            error: null
                          }

/*const clickedAuth = (state = 'clickedAuth' , action) => {
	switch (action.type){
		case A.CLICKED_LOGIN:
		case A.CLICKED_SIGNUP:
			return action.clickedLoginSignup
		case A.CLICKED_LOGOUT:
			return action.clickedLogout
		default:
			return state
	}
}*/

export const clickedLogin = (state = 'clickedAuth' , action) => {
	switch (action.type){
		 case A.CLICKED_LOGIN:
		     return action.userCredentials
     default:
			return state
	}
}

export const updateUserInfo = (userAuthState = defaultStartState , action) => {
  switch (action.type){

    case A.SIGNUP_REQUEST:
    case A.LOGIN_REQUEST:
    case A.LOGOUT_REQUEST:
      return Object.assign({}, userAuthState, {
        isFetching: true
      });

    case A.SIGNUP_SUCESS:
    case A.LOGIN_SUCCESS:
      return Object.assign({}, userAuthState, {
        isLoggedIn: true,
        isFetching: false,
        userObject: action.token,
        error: null
      });

    case A.LOGOUT_SUCCESS:
	    return Object.assign({}, userAuthState, {
	        isLoggedIn: false,
	        isFetching: false,
	        userObject: null,
	        error: null
	      });  

	case A.SIGNUP_FAIL:
    case A.LOGIN_FAIL:
	case A.LOGOUT_FAIL:
    	 return Object.assign({}, userAuthState, {
	        isFetching: false,
	        error: action.error
	      });    

 default: 
      return userAuthState;
  }
}