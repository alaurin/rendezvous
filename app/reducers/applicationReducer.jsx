import { combineReducers } from 'redux'
import {routerReducer} from "react-router-redux";
import {reducer as formReducer} from "redux-form";
import { user } from './user'
import { clickedLogin, updateUserInfo } from './authentification'

export default combineReducers({
  user,
  clickedLogin, 
  updateUserInfo,
  routing: routerReducer,
  form: formReducer
})