import {ADD_USER, EDIT_USER, DELETE_USER, LOAD_ALL_USERS} from "../actions/user"; 

const comparator = (u1, u2) => {
    if (u1.username == u2.username) {
        return 0;
    } else {
        return u1.username < u2.username ? -1 : 1;
    }
};

export function users(state=[], action){
    switch (action.type) {
      case ADD_USER:
            return addUser(state, action);
        case EDIT_USER:
            return editUser(state, action);
        case DELETE_USER:
            return deleteUser(state, action);
        case LOAD_ALL_USERS:
            return loadAllUsers(state, action);
        default:
            return state;
    }
}

const addUser = (state, action) => {

    let newUser = action.data;

    return [...state, newUser].sort(comparator);
};

const editUser = (state, action) => {

    let previousId = action.id;
    let updatedUser = action.data;

    let newState = state.map((user) => {
        if (user.id === previousId) {
            return Object.assign({}, user, updatedUser);
        } else {
            return user;
        }
    });

    return newState.sort(comparator);
};

const deleteUser = (state, action) => {

    let deletedId = action.id;
    let newState = [];

    state.forEach((user) => {
        if (user.id !== deletedId) {
            newState.push(user);
        }
    });

    return newState.sort(comparator);
};

const loadAllUsers = (state, action) => {
    let loadedUsers = action.data;
    if (loadedUsers !== undefined)
        return loadedUsers.sort(comparator);
    else
        return [];
};
