import React,{ Component, PropTypes } from 'react'
import {Link} from "react-router"
import { connect } from 'react-redux'
import {replace} from "react-router-redux"
import {reduxForm,SubmissionError} from "redux-form"
import LoginForm from './../presentational/LoginForm'
import {tryLogin} from './../../actions/log'

class Login extends Component{

	constructor(props) {
		super(props)
	}

	componentDidMount(){
		//const { dispatch, clickedLogin } = this.props
	}

	componentWillUnmount(){}

	render() {
    	//const { selectedSubreddit, posts, isFetching, lastUpdated } = this.props

    	return (
			<LoginForm /*onSubmit={this.handleSubmit}*//>
    )}
}

Login.contextTypes = {
    store: PropTypes.object.isRequired
}
const handleSubmit = (credentials, dispatch) => {
		//console.log(credentials)
		 //const { dispatch, clickedLogin } = this.props
		// dispatch(tryLogin(clickedLogin))
		/*const { store } = this.context
        return new Promise((resolve, reject) => {
            const unsubscribe = store.subscribe(() => {
                const state = store.getState()
                const status = state.auth.status

                if (status === 'success' || status === 'failure') {
                    unsubscribe()
                    status === 'success' ? resolve() /*dispatch(replace('/userHome')) : reject(state.auth.error)
                }
            })
            dispatch(tryLogin(credentials))
        }).bind(this) */
         dispatch(tryLogin(credentials))
         console.log("after dis")
	   
	}
const validate = values => {
  	const errors = {}
  	const emailRegex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/

    // email
    if (!values.email)
        errors.email = 'login.form.email.error.empty';
	
    else if(!values.email.match(emailRegex))
        errors.email ='login.form.email.error.invalid.email';

      // password
    if (!values.password){
              errors.password = 'login.form.password.error.empty';
    } else {
        // validate password for existing user
        if (values.password.length > 0 && values.password.length < 6)
            errors.password = 'login.form.password.error.tooshort';
        }

      return errors;
}

export default (reduxForm({
  form: 'login',
  onSubmit: handleSubmit,
  validate  
})(LoginForm))
