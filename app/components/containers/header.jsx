import React,{ Component , PropTypes} from 'react'
import { Button, ButtonGroup, Col, Dropdown, Glyphicon, Grid, MenuItem, Image, Row, Thumbnail } from 'react-bootstrap'
import {Link, IndexLink} from 'react-router'
import Auth from './../../modules/Auth';

class Header extends Component{

	render(){
		return(
			 <Grid>
			    <Row className="show-grid" id = 'header' >
			      	<Col xs={12} md={8}>
						<Image width={64} height={64} src='/img/licorne.jpg' alt="Image" responsive/>
			      	</Col>

			      	<Col xs={6} md={4}>
			      		<ButtonGroup>
							<Button><IndexLink to="/"><Glyphicon glyph="home"/></IndexLink></Button>
							<Dropdown id="nav-dropdown">
								<Dropdown.Toggle>
									<Glyphicon glyph="user"/>
								</Dropdown.Toggle>
								<Dropdown.Menu>
								{Auth.isUserAuthenticated() ? (
									<li><Link to="/logout">Log out</Link></li>
								) : (
								<div>
									<li><Link to="/login">Log in</Link></li>	
									<li><Link to="/signup">Sign up</Link></li>	
									</div>
								)}
								</Dropdown.Menu>
					        </Dropdown>
						</ButtonGroup>
			      	</Col>
			    </Row>
   			 </Grid>
			

		)
	}
}

                   
export default Header