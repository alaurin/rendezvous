import React, { Component, PropTypes } from 'react'
import {Field} from "redux-form"
import {Link} from "react-router"
import {Button,Col, Grid, FormGroup, FormControl, ControlLabel, ButtonToolbar, HelpBlock, PageHeader, Row} from 'react-bootstrap'

const renderField = field => 
			<div>
		      <FormControl {...field.input} type={field.type} placeholder={field.placeholder}/>
		      <FormControl.Feedback/>
		      {field.meta.touched && field.meta.error && <HelpBlock>{field.meta.error}</HelpBlock>}
			</div>
		

class LoginForm extends Component {

	 

	render () {
		const {error, handleSubmit, pristine, reset, submitting} = this.props

		 return (
		 	 <Grid>
			    <Row className="show-grid" id ='login' >
			      	<Col xs={12} md={8}>
						<PageHeader>Example login page header <small>Subtext for header</small></PageHeader>
			            <form onSubmit={handleSubmit(this.props.onSubmit)}>
				            <FormGroup>
				      			<ControlLabel htmlFor="login-form-email">"login.form.email.label"</ControlLabel>
				                <Field name="email" type="email" placeholder="example@example.com" component={renderField} />
							</FormGroup>

			               <FormGroup>
			     				<ControlLabel htmlFor="login-form-password">"login.form.password.label"</ControlLabel> 
			     				<Field name="password" type="password" placeholder="login.form.password.label" component={renderField}/>
							</FormGroup>

			               <ButtonToolbar className="form-button-toolbar">
			                        <Button type="submit" bsStyle="success" onClick={(e)=>{if(submitting) console.log("its submitting")/*e.preventDefault()*/}} disabled={submitting}>{/*i18n.t*/('login.form.button')}</Button>
			               </ButtonToolbar>
			               <br/>
			               <Link to="/signup" bsStyle="link">Don't have an account ? Sign Up !</Link>
			            </form>		
			      	</Col>
			    </Row>
   			 </Grid>
	    )
	}

}

LoginForm.propTypes = {
	handleSubmit:PropTypes.func.isRequired
}

export default LoginForm