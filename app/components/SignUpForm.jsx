import { Component, PropTypes } from 'react';
import {Link} from "react-router";
import {reduxForm} from "redux-form";
import {FormGroup, FormControl, ControlLabel, Button, ButtonToolbar, HelpBlock} from "react-bootstrap";
//import {translate} from "react-i18next";


/*
class SignUpForm extends React.Component {

    doSubmit(data, dispatch) {

        var action;

        // decide if this will be a "create" or "update" operation
        if (data.id === undefined) {
            action = addUser(data);
        } else {
            action = editUser(data.id, data);
        }

        return new Promise((resolve, reject) => {

            // execute ajax
            dispatch(action)

                .done(() => {
                    // resolve success
                    resolve();
                    // redirect user to /users
                    dispatch(replace('/users'));
                })

                // ajax error
                .fail(() => reject());
        });
    }

    render() {

        const {i18n} = this.context;

        // get form field values
        const {groups, availablePermissions, fields: {id, username, name, password, group, permissions}, error, handleSubmit, submitting} = this.props;

        const permissionOptions = [
            {key:'inherited', message:i18n.t('usermanager.permissions.form.option.notset')},
            {key:'allowed', message:i18n.t('usermanager.permissions.form.option.allowed')},
            {key:'denied', message:i18n.t('usermanager.permissions.form.option.denied')}
        ];

        const title = (id && id.value) ? i18n.t('usermanager.users.form.title.edit') : i18n.t('usermanager.users.form.title.create');

        const validationState = {
            name: name.touched ? (name.error ? "error" : "success") : null,
            username: username.touched ? (username.error ? "error" : "success") : null,
            password: password.touched ? (password.error ? "error" : "success") : null,
            group: group.touched ? (group.error ? "error" : "success") : null
        };

        return (
            <div>
                <h1 className="users-title">{title}</h1>
                <form onSubmit={handleSubmit(this.doSubmit)}>
                    <FormGroup validationState={validationState.name}>
                        <ControlLabel htmlFor="users-form-name">{i18n.t('usermanager.users.form.name.label')}</ControlLabel>
                        <FormControl id="users-form-name" type="text" disabled={submitting} {...name}/>
                        <FormControl.Feedback/>
                        {name.touched && name.error && <HelpBlock>{i18n.t(name.error)}</HelpBlock>}
                    </FormGroup>
                    <FormGroup validationState={validationState.username}>
                        <ControlLabel htmlFor="users-form-username">{i18n.t('usermanager.users.form.username.label')}</ControlLabel>
                        <FormControl id="users-form-username" disabled={submitting} {...username}/>
                        <FormControl.Feedback/>
                        {username.touched && username.error && <HelpBlock>{i18n.t(username.error)}</HelpBlock>}
                    </FormGroup>
                    <FormGroup validationState={validationState.password}>
                        <ControlLabel htmlFor="users-form-password">{i18n.t('usermanager.users.form.password.label')}</ControlLabel>
                        <FormControl id="users-form-password" type="password" placeholder={id && id.value ? i18n.t('usermanager.users.form.password.placeholder') : '' } disabled={submitting} {...password}/>
                        <FormControl.Feedback/>
                        {password.touched && password.error && <HelpBlock>{i18n.t(password.error)}</HelpBlock>}
                    </FormGroup>
                    <FormGroup validationState={validationState.group}>
                        <ControlLabel htmlFor="users-form-group">{i18n.t('usermanager.users.form.group.label')}</ControlLabel>
                        <FormControl componentClass="select" id="users-form-group" disabled={submitting} {...group}>
                            <option value="">{``}</option>
                            {groups.map((group) =>
                                <option key={group.id} value={group.id}>{group.name}</option>
                            )}
                        </FormControl>
                        {group.touched && group.error && <HelpBlock>{i18n.t(group.error)}</HelpBlock>}
                    </FormGroup>
                    <PermissionForm availablePermissions={availablePermissions} permissions={permissions} disabled={submitting} permissionOptions={permissionOptions}/>
                    {error && <p className="has-error bg-danger" style={{padding:'15px'}}>{i18n.t(error)}</p>}
                    <ButtonToolbar className="form-button-toolbar">
                        <Button type="submit" bsStyle="success" disabled={submitting}>{i18n.t('usermanager.users.form.button.save')}</Button>
                        <Link to="/users" className="btn btn-default" onClick={(e)=>{if(submitting)e.preventDefault()}} disabled={submitting}>{i18n.t('usermanager.users.form.button.cancel')}</Link>
                    </ButtonToolbar>
                </form>
            </div>
        )
    }
}

UserForm.propTypes = {
    fields: React.PropTypes.object.isRequired,
    handleSubmit: React.PropTypes.func.isRequired,
    error: React.PropTypes.string,
    submitting: React.PropTypes.bool.isRequired
};

UserForm.contextTypes = {
    router: React.PropTypes.object.isRequired,
    i18n: React.PropTypes.object
};


const mapStateToProps = (state, ownProps) => {

    var values = {
        name: '',
        username: '',
        password: '',
        group: '',
        permissions: []
    };

    // find existing user based on user id
    if (ownProps.params.userId !== undefined) {
        state.users.forEach((user)=> {
            if (user.id === ownProps.params.userId) {
                // create a copy to prevent modification of original data
                values = Object.assign({}, values, user);
            }
        });
    }

    // fix permissions
    values.permissions = permissionMergeAndSort(values.permissions, state.permissions);

    return {
        initialValues: values,
        groups: state.groups, // get list of all existing groups
        availablePermissions: state.permissions
    }
};

export default translate()(reduxForm({
    form: 'user', // unique name for this form
    fields: fields, // all the fields in the form
    validate(user) {
        var errors = {};
        const letters = /^[A-Za-z0-9]+$/;

        // username
        if (!user.username)
            errors.username = 'usermanager.users.form.username.error.empty';

        else if(!user.username.match(letters))
            errors.name ='usermanager.users.form.username.error.invalid.charaters';

        // name
        if (!user.name)
            errors.name = 'usermanager.users.form.name.error.empty';

        // group
        if (!user.group)
            errors.group = 'usermanager.users.form.group.error.empty';

        // password
        if (!user.id) {
            // validate password for a new user
            if (!user.password)
                errors.password = 'usermanager.users.form.password.error.empty';
            else if (user.password.length < 6)
                errors.password = 'usermanager.users.form.password.error.tooshort';
        } else {
            // validate password for existing user
            if (user.password.length > 0 && user.password.length < 6)
                errors.password = 'usermanager.users.form.password.error.tooshort';
        }

        return errors;
    }
}, mapStateToProps)(UserForm));

*/