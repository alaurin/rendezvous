import React,{ Component, PropTypes } from 'react'
import {Link} from "react-router"
import {reduxForm, Field,SubmissionError} from "redux-form"
import {replace} from "react-router-redux"
import {FormGroup, FormControl, ControlLabel, Button, ButtonToolbar, HelpBlock} from "react-bootstrap"

//import {translate} from "react-i18next"; //TODO:TRADUCTION

const validate = values => {
  	const errors = {}
  	const emailRegex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i

      // email
    if (!values.email)
        errors.email = 'login.form.email.error.empty';

    else if(!values.email.match(emailRegex))
        errors.name ='login.form.email.error.invalid.charaters';

      // password
    if (!values.password){
              errors.password = 'login.form.password.error.empty';
    } else {
        // validate password for existing user
        if (values.password.length > 0 && values.password.length < 6)
            errors.password = 'login.form.password.error.tooshort';
        }

      return errors;
}
	 
const renderField = props => (
	<div>
      <FormControl {...props} />
      <FormControl.Feedback/>
      {props.touched && props.error && <HelpBlock>{props.error}</HelpBlock>}
	</div>
)

const SubmitLoginForm = (props) => {
	const {error, onSubmit :handleSubmit, pristine, reset, submitting} = props

	 return (
         <div>
            <form onSubmit={handleSubmit/*(e) => this.handleSubmit(e)*/}>
	            <FormGroup>
	      			<ControlLabel htmlFor="login-form-email">"login.form.email.label"</ControlLabel>
	                <Field name="email" type="email" placeholder="login.form.email.label" component={renderField} />
				</FormGroup>

               <FormGroup>
     				<ControlLabel htmlFor="login-form-password">"login.form.password.label"</ControlLabel> 
     				<Field name="password" type="password" component={renderField} placeholder="login.form.password.label"/>
				</FormGroup>

               <ButtonToolbar className="form-button-toolbar">
                        <Button type="submit" bsStyle="success" onClick={(e)=>{if(submitting)e.preventDefault()}} disabled={submitting}>{/*i18n.t*/('login.form.button')}</Button>
                        <Link to="/signup" onClick={handleClick} bsStyle="link">Don't have an account ? Sign Up !</Link>
               </ButtonToolbar>
            </form>
         </div>
      )
}

const mapStateToProps = (state, ownProps) => {

    var values = {
        email: '',
        password: '',
    };

    return {
        initialValues: values
    }
}

export default (reduxForm({
  form: 'login',
  validate  
}, mapStateToProps)(SubmitLoginForm))