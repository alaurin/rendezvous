export function addUser(user) {

    const data = JSON.stringify(user);

    return function (dispatch) {

        dispatch(showLoadingDialog('addUser'));

        return jQuery.ajax({url: baseUrl + '/api/users', method: 'post', contentType, headers, data})

            .done(function (response) {
                dispatch({type: ADD_USER, data: response});
            })

            .fail(function (error) {
                // resolve error
                switch (error.status) {
                    case 422:
                        dispatch(showMessageDialog("usermanager.dialog.title.error", "usermanager.api.user.error.existing"));
                        break;
                    case 403:
                        dispatch(showMessageDialog("usermanager.dialog.title.error", "usermanager.server.authenticationerrror"));
                        break;
                    case 401:
                        dispatch(showMessageDialog("usermanager.dialog.title.error", "usermanager.server.permissionerror"));
                        break;
                    case 400:
                        dispatch(showMessageDialog("usermanager.dialog.title.error", "usermanager.api.user.error.invalidid"));
                        break;
                }
            })

            .always(function () {
                dispatch(hideLoadingDialog('addUser'));
            });
    }
}