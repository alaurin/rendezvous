import fetch from 'isomorphic-fetch'
import thunk from 'redux-thunk'
import * as A from './action-type'
import Auth from './../modules/Auth'



const contentType = 'application/json; charset=utf-8';


export const loginClicked = userCredentials =>{
	return{
		type: A.CLICKED_LOGIN,
		userCredentials
	}
}

const loginSuccess = token => {
   return {
      type: A.LOGIN_SUCCESS,
      token
   }
}

const loginFail = error => {
   return {
      type: A.LOGIN_FAIL,
      error
   }
}

const loginRequest = userCredentials =>{

  return{
    type: A.LOGIN_REQUEST,
    userCredentials
  }
}

export const tryLogin = userCredentials => {
	const credentials = JSON.stringify(userCredentials)
	return dispatch => {
		dispatch(loginRequest(userCredentials))
		
		//Try to LOGIN
		return fetch('/authentication/login',{
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			body: credentials
		})
			.then(checkStatus)
			.then(parseJSON)
			.then(treatResponse)
			.catch(error => dispatch(loginFail(error)))
	}

}


const checkStatus = response => {
  if (response.status >= 200 && response.status < 300) {
    return response
  } else {
    var error = new Error(response.statusText)
    error.response = response
    throw error
  }
}

const parseJSON = response => response.json() 

const treatResponse = data => {
	console.log(data)
	Auth.authenticateUser(data.token)
	console.log(data)
	dispatch(loginSuccess(Auth.getToken()))
}










