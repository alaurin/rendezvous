// webpack.config.js
var webpack = require('webpack');
var path = require('path');

module.exports = {

    entry: [

        './index'
    ],

    output: {
        path: path.join(__dirname, './../assets/js'),
        filename: 'application.js',
    },

    debug: true,
    devtool: '#source-map',
    resolve: {
        root: path.resolve('./RendezVous'),
        modulesDirectories: ['./node_modules'],
        extensions: ['', '.js', '.jsx']
    },

    plugins: [
        // new webpack.optimize.UglifyJsPlugin({
        //     compressor: {
        //         warnings: false
        //     }
        // })
        // DEV MODE
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': '"development"'
        })
    ],

    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react', 'stage-2'],
                    cacheDirectory: true
                }
            }
        ]
    }
};