import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import {Router, Route,browserHistory, IndexRoute} from 'react-router'
import {syncHistoryWithStore} from 'react-router-redux'
//import {I18nextProvider} from "react-i18next";
//import i18n from "./i18n";
import configureStore from './store/configureStore'
import App from './components/containers/App'
import {loginRequest} from './actions/log'
import Login from './components/containers/Login'
import Home from './components/containers/Home'
import routes from './routes/routes';

const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);

render(
  //<I18nextProvider i18n={i18n}>
        <Provider store={store}>
            <Router history={history}>
            routes = {routes}
               { /*<Route path="/" component={App}>
                <IndexRoute component={Home}/>
                <Route path="/login" component={Login}/>
                </Route>*/}
            </Router>
        </Provider>,
    //</I18nextProvider>,
   document.getElementById('root')
);

// Load all initial states
//store.dispatch(loginRequest());