import App from './../components/containers/App'
import Home from './../components/containers/Home'
import Login from './../components/containers/Login'
import SignUp from './../components/containers/SignUp'
//import Dashboard from '../components/containers/Dashboard'
import Auth from './../modules/Auth'

const routes = {
	component: App,
	childRoutes: [

		{
			path: '/',
			getComponent: (location, callback) => {
				if (Auth.isUserAuthenticated()) {
					//callback(null, Dashboard);
				} else {
					callback(null, Home);
				}
			}
		},

		{
			path: '/login',
			component: Login
		},

		{
			path: '/signup',
			component: SignUp
		},

		{
			path: '/logout',
			onEnter: (nextState, replaceState) => {
				Auth.logoutUser();

				// change the current URL to /
				replaceState(null, '/');
			}
		}
	]
}

export default routes