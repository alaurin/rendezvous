// ============== Module dependencies ============== //
var app = require('./app');
var debug = require('debug')('appointment-scheduler:server');
var http = require('http');


// ============== Port from env to Express  ============== //
var port = normalizePort(process.env.PORT || '6969');
app.set('port', port);


// ============== HTTP server ============== //
var server = http.createServer(app);

server.listen(port);
console.log(server.address());

//set event listener
//server.on('error', onError);
//server.on('listening', onListening);


// Normalize port
function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server
 */
 
//function onError
//function onListening