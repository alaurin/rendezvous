// ============== Package ============== //
const express = require('express'),
 path = require('path'),
 passport = require('passport'),
 config = require('./config/environment'),
 bodyParser = require('body-parser'),
 morgan = require('morgan'),
 router = require('express').Router(),
 User = require('./models/user')
 require('es6-promise').polyfill()


let app = express();

// secret variable
app.set('superSecret', config.session.secret); 

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// use morgan to log requests to the console
app.use(morgan('dev'));

app.use(passport.initialize());

// ============== Serve Static Files ============== //
app.use(express.static(path.join(__dirname, './../assets')));
app.use(express.static(path.join(__dirname, './../app')));



// ============== Load local config files ============== //
require('./config/passport/index')(config);
//require('./config/express').init(app);
//require('./config/passport').init(app);

// ============== Middlewares ============== //
const authCheckMiddleware = require('./middlewares/auth')(config);
app.use('/api', authCheckMiddleware);

// === Set Up Routes === //
app.get('/setup', function(req, res) {

  // create a sample user
  var andrea = new User({ 
    name: 'Andrea Laurin', 
    email: 'andreanne6@live.ca',
    password: 'password',
    homePhone: '55555555' 
  });

  // save the sample user
  andrea.save(function(err) {
    if (err) throw err;

    console.log('User saved successfully');
    res.json({ success: true });
  });
});

// ============== BASIC ROUTES ============== //
app.use(require('./routes/main'));
app.use('/authentication', require('./routes/auth'));

// ============== API ROUTES ============== //
app.use('/api', require('./routes/api'));


module.exports = app;