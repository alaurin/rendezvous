var passport = require('passport');
module.exports.init = function(app) {
	
	var env = app.get('env');
	var root = app.get('root');
	
	//app.configure(function() {
		//Passport session
		app.use(passport.initialize());
		app.use(passport.session());
	//});
}