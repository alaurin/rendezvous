const LocalStrategy = require('passport-local').Strategy,
  mongoose = require('./../../models/db'),
  User = require('./../../models/user');

module.exports = function(config) {
	return new LocalStrategy(
		{
			usernameField: 'email',
			passwordField: 'password',
			session: false,
			passReqToCallback: true
		},

		function(req, email, password, done) {
			let userData = {
				email: email.trim(),
				password: password.trim(),
				name: req.body.name.trim(),
				homePhone: req.body.name.trim(),
				cellPhone: req.body.name.trim(),
				createdAt: req.body.name.trim()
			};

			let newUser = new User(userData);

			newUser.save(function(err) {
				if (err) { 
					return done(err); 
				}

				return done(null);
			});
		}
	);
};