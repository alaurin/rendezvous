const passport = require('passport');

module.exports = function(config) {

  // loading strategies
  const signupStrategy = require('./signup')(config);
  const loginStrategy = require('./login')(config);

  passport.use('signup', signupStrategy);
  passport.use('login', loginStrategy);

};