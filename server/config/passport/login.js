const bcrypt = require('bcrypt'),
 jwt = require('jsonwebtoken'),
 LocalStrategy = require('passport-local').Strategy,
 User = require('./../../models/user');

module.exports = function(config) {
	return new LocalStrategy({
		usernameField: 'email',
		passwordField: 'password',
		session: false,
    	passReqToCallback: true
		},

		function(req, username, password, done) {
			
			let userData = {
				email: username.trim(),
				password: password.trim()
    		}

    		// find a user by email address
			User.findOne({email: userData.email}, function(err, user) {
				if (err) { 
					return done(err);
				}

				if (!user) {
					let error = new Error("Incorrect email or password")
					error.name = "IncorrectCredentialsError"
					return done(error)
				}	

				// Check password with value from db
				user.comparePassword(userData.password, function(err, isMatch) {

					if (err) { 
						return done(err)
					}

					if (!isMatch) {
						let error = new Error("Incorrect email or password")
						error.name = "IncorrectCredentialsError"
						return done(error)
					}


					let payload = {
						sub: user._id,
					};

					// create a token string
					let token = jwt.sign(payload, req.app.get('superSecret'), {
			    		expiresIn: 1440 // expires in 24 hours
			    	});

					let userData = {
					name: user.name
					};

					return done(null, token, userData)
				})
			})
		}
	)
}