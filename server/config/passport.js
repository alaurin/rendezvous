//passport.js

'use strict';

var passport = require('passport');
var mongoose = require('./../models/db');
var User = require('./../models/user');

module.exports.init = function(app) {
  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    User.findById(id, done);
  });

  // Load strategies
  require('./passport/login')();
  //require('./strategies/signup')();
};