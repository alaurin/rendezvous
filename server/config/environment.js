'use strict';

module.exports = {
  port: 3000,
  hostname: '127.0.0.1',
  baseUrl: 'http://localhost:3000',
  mongodb: {
    uri: 'mongodb://admin:admin@ds161315.mlab.com:61315/appointment_scheduler',
    timeOut:5000,
  },
  app: {
    name: 'appointment-scheduler'
  },
  serveStatic: true,
  session: {
    secret: 'mySecretIsGreat'
  },
  proxy: {
    trust: true
  },
  swig: {
    cache: false
  },
};