var mongoose = require('mongoose'),
config = require('./../config/environment'); 

// Database connection string 
var dbURI = config.mongodb.uri; 

// Database connect options
var options = { replset: { socketOptions: { connectTimeoutMS : config.mongodb.timeOut }}};

// Create the database connection 
mongoose.connect(dbURI, options); 

// CONNECTION EVENTS

// When successfully connected
mongoose.connection.on('connected', function () {  
  console.log('Mongoose default connection open to ' + dbURI);
}); 

// If the connection throws an error
mongoose.connection.on('error',function (err) {  
  console.log('Mongoose default connection error: ' + err);
}); 

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {  
  console.log('Mongoose default connection disconnected'); 
});

// If the Node process ends, close the Mongoose connection 
process.on('SIGINT', close);
process.on('SIGTERM', close);
process.on('SIGHUP', close);

function close() {  
  mongoose.connection.close(function () { 
    console.log('Mongoose default connection disconnected through app termination'); 
    process.exit(0); 
  }); 
}

module.exports = mongoose;