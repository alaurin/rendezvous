// ============== Package ============== //
var express = require('express'),
    router = express.Router(),
  	User = require('./../../models/mockUser'),
  	token = require('./../../middlewares/verifyToken')


// ============== Get Users ============== //
router.get('/use',token, function(req, res) {
	User.find({}, function(err, users) {
		res.json(users);
	});
});

module.exports = router
