// ============== Package ============== //
var express = require('express'),
    router = express.Router(),
 	path = require('path')

// ============== Local Packages ============== //
var auth = require('./authenticate'),
getUsers = require('./getUsers')

// ============== Load Front-end HTML ============== //
// router.get('/dashboard', function(req, res, next) {
// 	res.json({message: 'Yeah you are auth'});
// });

//router.use('/main',main)
router.use(auth)
router.use(getUsers)



module.exports = router 

