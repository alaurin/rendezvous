/*   createPageRouter() {
      const router = express.Router();  
      // respond with index page to ANY request  
      router.get('*', (req, res) => { 
          res.render('index');
    });
return router;
},

createApiRouter(app) {
    const router = express.Router();
    router.get('/latest-bills', (req, res) => {
        this.retrieveLatestBills((err, content) => {
            if(!err) {
                res.json(JSON.parse(content));                                    
            } else {
                res.status(500).send();
            }
        });
    });
return router;
} */

//Starts here

var auth = require('../middlewares/auth.js');

function addAuthRoute(app, passport, routePath, strategy) {
  app.post(routePath, function(req, res, next) {
    passport.authenticate(strategy, function(err, user, info) {
      if (err) { return next(err); }
      if (!user) { return res.json(info); }
      if (user) {
        req.logIn(user, function(err) {
          if (err) { return next(err); }
          return res.json(user); 
        });
      }
    })(req, res, next);
  });
}

module.exports = function(app, passport) {
  //addAuthRoute(app, passport, "/signup", "local-signup");

  addAuthRoute(app, passport, "/login", "login");

  app.post('/logout', auth.isAuthenticated, function(req, res) {
    req.logout();
    req.session.destroy();
    return res.json('logged out :)');
  });

  app.post('/checkSession', function(req, res) {
    var isLoggedIn = req.isAuthenticated();
    if (isLoggedIn) 
      return res.json(
        { isLoggedIn: isLoggedIn,
          userObject: { displayName: req.user.display_name, 
          id:req.user.id, 
          email:req.user.email
        }
      });
    
    return res.json({isLoggedIn: isLoggedIn});
  });
};