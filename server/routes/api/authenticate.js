// ============== Package ============== //
var express = require('express'),
    router = express.Router(),
  	User = require('./../../models/mockUser'),
	jwt = require('jsonwebtoken'),
	token = require('./../../middlewares/verifyToken')


// ============== Auth User ============== //

router.post('/authenticate', function(req, res) {

  // find the user
	User.findOne({
		name: req.body.name
	}, function(err, user) {

		if (err) throw err;

		if (!user) {
			res.json({ success: false, message: 'Authentication failed. User not found.' });
		} else if (user) {

			// check if password matches
			if (user.password != req.body.password) {
		  		res.json({ success: false, message: 'Authentication failed. Wrong password.' });
		  	} else {

			    // if user is found and password is right
			    // create a token
			    var token = jwt.sign(user, req.app.get('superSecret'), {
			    	expiresIn: 1440 // expires in 24 hours
			    });

			    // return the information including token as JSON
			    res.json({
			    	success: true,
			    	message: 'Enjoy your token!',
			    	token: token
			    });
		  	}   
		}
	});
});

// ============== Logout User ============== //
router.post('/logout', token, function(req, res) {
    req.logout();
    //req.session.destroy();
    return res.json('logged out :)');
  });

//router.get('/login', function(req, res) {
  
    //req.session.destroy();
  // return res.json('logged out :)');
//  });

// ============== Check session User ============== // 
/*
 router.post('/checkSession', function(req, res) {

    var isLoggedIn = req.body.token || req.query.token || req.headers['x-access-token'];
    if (isLoggedIn) 
      return res.json(
        { isLoggedIn: isLoggedIn,
          userObject: { displayName: req.user.display_name, 
          id:req.user.id, 
          email:req.user.email
        }
      });
    
    return res.json({isLoggedIn: isLoggedIn});
  });*/

module.exports = router

