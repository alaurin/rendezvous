const express = require('express'),
	//validator = require('validator')
	router = express.Router()
	passport = require('passport')

router.post('/signup', function(req, res, next) {

	passport.authenticate('signup', function(err, info) {
		if (err) {
			if (err.name === "MongoError" && err.code === 11000) {
			// the 11000 Mongo code is for duplicate email error
			// the 409 HTTP status code is for conflict error
			return res.status(409).json({ success: false, message: "Check the form for errors.", errors: { email: "This email is already taken." } });
			}

			return res.status(400).json({ success: false, message: "Could not process the form." });
		}

		return res.status(200).json({ success: true, message: 'You have successfully signed up! Now you should be able to log in.' });
	})(req, res, next);

});

router.post('/login', function(req, res, next) {

	passport.authenticate('login', function(err, token, userData) {
		if (err) {
			if (err.name === "IncorrectCredentialsError") {
				return res.status(400).json({ success: false, message: err.message });
			}

			return res.status(400).json({ success: false, message: "Could not process the form." });
		}

		return res.json({ success: true, message: "You have successfully logged in!", token: token, user: userData });
	})(req, res, next);
});

module.exports = router;